package com.kilapdinas.kilapdinas;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.Model.ModelDataLaporan;
import com.kilapdinas.kilapdinas.Model.ModelHistoryLaporan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterHistoryLaporan extends RecyclerView.Adapter<AdapterHistoryLaporan.HolderItem> {
    private Context context;
    private List<ModelHistoryLaporan> datas;
    public AdapterHistoryLaporan(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public HolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_history_laporan, null);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @Override
    public void onBindViewHolder(@NonNull HolderItem holder, int i) {
        final ModelHistoryLaporan x = datas.get(i);

        holder.tvJns.setText(x.getJns_bencana());
        holder.tvAlamat.setText("Alamat bencana : "+x.getAlamat());
        holder.tvKat.setText("Kategori bencana : "+x.getKat_bencana());
        if (x.getStatus_survey() == 1) {
            holder.tvStatus.setText("Laporan sudah diterima");
            holder.ivStatus.setImageResource(R.drawable.ic_check_circle_black_24dp);
        } else if (x.getStatus_survey() == 2) {
            holder.tvStatus.setText("Laporan sedang proses ditindaklanjuti");
            holder.ivStatus.setImageResource(R.drawable.ic_schedule_blue_24dp);
        } else if (x.getStatus_survey() == 3){
            holder.tvStatus.setText("Laporan telah selesai ditindaklanjuti");
            holder.ivStatus.setImageResource(R.drawable.ic_check_circle_green_24dp);
        } else {
            holder.tvStatus.setText("Laporan tidak diterima");
            holder.ivStatus.setImageResource(R.drawable.ic_close_red_24dp);
        }
        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (x.getStatus_survey() == 1) {
                    Intent intent = new Intent(context, DetailHistoryTerimaActivity.class);
                    intent.putExtra("id", x.getId_bencana());
                    intent.putExtra("kat", x.getKat_bencana());
                    intent.putExtra("jns", x.getJns_bencana());
                    intent.putExtra("alamat", x.getAlamat());
                    intent.putExtra("lat", x.getLat());
                    intent.putExtra("lng", x.getLng());
                    intent.putExtra("ket", x.getKet());
                    intent.putExtra("image", x.getImage());
                    intent.putExtra("tgl_bencana", x.getTgl_bencana());
                    intent.putExtra("tgl_lapor", x.getTgl_pelaporan());
                    intent.putExtra("id_survey", x.getId_survey());
                    intent.putExtra("status", x.getStatus_survey());
                    intent.putExtra("tgl_hasil", x.getTgl_survey());
                    context.startActivity(intent);
                } else if (x.getStatus_survey() == 2) {
                    Intent intent = new Intent(context, DetailHistoryProsesActivity.class);
                    intent.putExtra("id", x.getId_bencana());
                    intent.putExtra("kat", x.getKat_bencana());
                    intent.putExtra("jns", x.getJns_bencana());
                    intent.putExtra("alamat", x.getAlamat());
                    intent.putExtra("lat", x.getLat());
                    intent.putExtra("lng", x.getLng());
                    intent.putExtra("ket", x.getKet());
                    intent.putExtra("image", x.getImage());
                    intent.putExtra("tgl_bencana", x.getTgl_bencana());
                    intent.putExtra("tgl_lapor", x.getTgl_pelaporan());
                    intent.putExtra("id_survey", x.getId_survey());
                    intent.putExtra("status", x.getStatus_survey());
                    intent.putExtra("tgl_hasil", x.getTgl_survey());
                    context.startActivity(intent);
                } else {
                    Intent intent = new Intent(context, DetailHistorySBActivity.class);
                    intent.putExtra("id", x.getId_bencana());
                    intent.putExtra("kat", x.getKat_bencana());
                    intent.putExtra("jns", x.getJns_bencana());
                    intent.putExtra("alamat", x.getAlamat());
                    intent.putExtra("lat", x.getLat());
                    intent.putExtra("lng", x.getLng());
                    intent.putExtra("ket", x.getKet());
                    intent.putExtra("image", x.getImage());
                    intent.putExtra("tgl_bencana", x.getTgl_bencana());
                    intent.putExtra("tgl_lapor", x.getTgl_pelaporan());
                    intent.putExtra("id_survey", x.getId_survey());
                    intent.putExtra("status", x.getStatus_survey());
                    intent.putExtra("tgl_hasil", x.getTgl_survey());
                    intent.putExtra("ket_hasil", x.getKet_survey());
                    intent.putExtra("image_hasil", x.getImage_survey());
                    context.startActivity(intent);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setDatas(List<ModelHistoryLaporan> datas) {
        this.datas = datas;
    }

    public List<ModelHistoryLaporan> getDatas() {
        return datas;
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        @BindView(R.id.tv_jns)
        TextView tvJns;
        @BindView(R.id.tv_alamat)
        TextView tvAlamat;
        @BindView(R.id.tv_kat)
        TextView tvKat;
        @BindView(R.id.tv_status)
        TextView tvStatus;
        @BindView(R.id.iv_status)
        ImageView ivStatus;

        public HolderItem(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
