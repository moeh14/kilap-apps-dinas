package com.kilapdinas.kilapdinas;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

import static android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.fl_container)
    FrameLayout flContainer;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    Toolbar toolbarMain;
    TextView tvTitleToolbar, tvBadge;
    View badge;

    public static final String FRAGMENT_HOME = "FRAGMENT_HOME";
    public static final String FRAGMENT_OTHER = "FRAGMENT_OTHER";

//    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
//                    mTextMessage.setText("Home");
                    viewFragment(new HomeFragment(), FRAGMENT_HOME);
                    return true;
                case R.id.navigation_data_laporan:
//                    mTextMessage.setText("Data Laporan");
                    viewFragment(new DataLaporanFragment(), FRAGMENT_OTHER);
                    return true;
                case R.id.navigation_history:
//                    mTextMessage.setText("History");
                    viewFragment(new HistoryFragment(), FRAGMENT_OTHER);
                    return true;
                case R.id.navigation_bantuan:






//                    mTextMessage.setText("Bantuan");
                    viewFragment(new BantuanFragment(), FRAGMENT_OTHER);
                    return true;
                case R.id.navigation_akun:
//                    mTextMessage.setText("Bantuan");
                    viewFragment(new AkunFragment(), FRAGMENT_OTHER);
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("KILAP APPS");

        viewFragment(new HomeFragment(), FRAGMENT_HOME);

//        mTextMessage = (TextView) findViewById(R.id.message);
//        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        setNotificationBadge();
        getLaporanMasuk();
    }

    public void getLaporanMasuk() {
        String url = URL.laporanMasuk;
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject datas = new JSONObject(new String(responseBody));
                    Log.e("laporanMasuk", datas.toString());
                    int laporanMasuk = datas.getInt("data");
                    if (laporanMasuk == 0) {
                        badge.setVisibility(View.GONE);
                    } else {
                        badge.setVisibility(View.VISIBLE);
                        tvBadge.setText(String.valueOf(laporanMasuk));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                badge.setVisibility(View.GONE);
            }
        });
    }

    private void setNotificationBadge() {
        BottomNavigationItemView itemView = navigation.findViewById(R.id.navigation_data_laporan);
        badge = getLayoutInflater().inflate(R.layout.component_tabbar_badge, null);
        tvBadge = badge.findViewById(R.id.notificationsBadgeTextView);
        itemView.addView(badge);
        badge.setVisibility(View.GONE);
    }

    private void viewFragment(Fragment fragment, String name){
        final FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fl_container, fragment);
        // 1. Know how many fragments there are in the stack
        final int count = fragmentManager.getBackStackEntryCount();
        // 2. If the fragment is **not** "home type", save it to the stack
        if( name.equals(FRAGMENT_OTHER) ) {
            fragmentTransaction.addToBackStack(name);
        }
        // Commit !
        fragmentTransaction.commit();
        Log.e("count", String.valueOf(count));
        Log.e("manager", String.valueOf(fragmentManager.getBackStackEntryCount()));
        // 3. After the commit, if the fragment is not an "home type" the back stack is changed, triggering the
        // OnBackStackChanged callback
        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                // If the stack decreases it means I clicked the back button
                if( fragmentManager.getBackStackEntryCount() <= count){
                    // pop all the fragment and remove the listener
                    fragmentManager.popBackStack(FRAGMENT_OTHER, POP_BACK_STACK_INCLUSIVE);
                    fragmentManager.removeOnBackStackChangedListener(this);
                    // set the home button selected
                    navigation.getMenu().getItem(0).setChecked(true);
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLaporanMasuk();
    }
}
