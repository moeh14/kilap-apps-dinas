package com.kilapdinas.kilapdinas;

import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.ConnectionDetector;
import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailHistoryProsesActivity extends AppCompatActivity {
    //data lapor
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_kat)
    TextView tvKat;
    @BindView(R.id.tv_jns)
    TextView tvJns;
    @BindView(R.id.tv_alamat)
    TextView tvAlamat;
    @BindView(R.id.tv_ket)
    TextView tvKet;
    @BindView(R.id.iv_bencana)
    ImageView ivBencana;
    @BindView(R.id.tv_tgl_bencana)
    TextView tvTglBencana;
    @BindView(R.id.tv_tgl_lapor)
    TextView tvTglLapor;
    //hasil
    @BindView(R.id.tv_status)
    TextView tvStatus;
    @BindView(R.id.tv_tgl_hasil)
    TextView tvTglHasil;
    @BindView(R.id.pb_load_image)
    ProgressBar pbLoad;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    private String id, kat, jns, alamat, ket, image, tglBencana, tglLapor, idSurvey, nama, tglHasil, ketHasil, imageHasil;
    private int status;
    double lat, lng;
    private SimpleDateFormat dateFormat;
    Date date, date1, date2;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_history_proses);
        ButterKnife.bind(this);

        //modal koneksi
        init_modal_bd_dialog();
        conn = new ConnectionDetector(DetailHistoryProsesActivity.this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("DETAIL LAPORAN");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Locale locale = new Locale("in");
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);

        Intent intent = getIntent();
        id = intent.getExtras().getString("id");
        kat = intent.getExtras().getString("kat");
        jns = intent.getExtras().getString("jns");
        alamat = intent.getExtras().getString("alamat");
        lat = intent.getExtras().getDouble("lat");
        lng = intent.getExtras().getDouble("lng");
        ket = intent.getExtras().getString("ket");
        image = intent.getExtras().getString("image");
        tglBencana = intent.getExtras().getString("tgl_bencana");
        tglLapor = intent.getExtras().getString("tgl_lapor");
        tglHasil = intent.getExtras().getString("tgl_hasil");
        try {
            date = dateFormat.parse(intent.getExtras().getString("tgl_bencana"));
            date1 = dateFormat.parse(intent.getExtras().getString("tgl_lapor"));
            date2 = dateFormat.parse(intent.getExtras().getString("tgl_hasil"));
            dateFormat.applyPattern("dd MMMM yyyy HH:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }

//        tglBencana = dateFormat.format();
//        tglLapor = intent.getExtras().getString("tgl_lapor");
        idSurvey = intent.getExtras().getString("id_survey");
        status =  intent.getExtras().getInt("status");
//        tglHasil = intent.getExtras().getString("tgl_hasil");

        Log.e("nama", ket);
        tvId.setText(id);
        tvKat.setText(kat);
        tvJns.setText(jns);
        tvAlamat.setText(alamat);
        tvKet.setText(ket);

        Picasso.with(getApplicationContext()).load(URL.rootImage + image)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(ivBencana, new Callback() {
                    @Override
                    public void onSuccess() {
//                        holder.loadingImage.setVisibility(View.GONE);
                        pbLoad.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        holder.loadingImage.setVisibility(View.GONE);
                        pbLoad.setVisibility(View.GONE);
                        ivBencana.setImageResource(R.drawable.ic_no_image);
                    }
                });
        tvTglBencana.setText(dateFormat.format(date));
        tvTglLapor.setText(dateFormat.format(date1));
        tvStatus.setText("Laporan sedang proses ditindaklanjuti");
        tvTglHasil.setText(dateFormat.format(date2));
//        if (status == 1) {
//            tvStatus.setText("Laporan sudah diterima");
//            tvTglHasil.setText(tglHasil);
//        }
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    @OnClick(R.id.rl_alamat)
    public void alamat(){
        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng+"?q="+lat+","+lng+"(lokasi bencana)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @OnClick(R.id.btn_selesai_tindak)
    public void btnSelesai(){
        Intent intent = new Intent(DetailHistoryProsesActivity.this, FormHasilTindakActivity.class);
        intent.putExtra("id", id);
        intent.putExtra("kat", kat);
        intent.putExtra("jns", jns);
        intent.putExtra("alamat", alamat);
        intent.putExtra("lat", lat);
        intent.putExtra("lng", lng);
        intent.putExtra("ket", ket);
        intent.putExtra("image", image);
        intent.putExtra("tgl_bencana", tglBencana);
        intent.putExtra("tgl_lapor", tglLapor);
        intent.putExtra("id_survey", idSurvey);
        intent.putExtra("status", status);
        intent.putExtra("tgl_hasil", tglHasil);
        startActivity(intent);
        finish();
    }
}
