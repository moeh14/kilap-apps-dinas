package com.kilapdinas.kilapdinas;

import android.content.Intent;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.kilapdinas.kilapdinas.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Date;
import java.text.SimpleDateFormat;

import cz.msebera.android.httpclient.Header;

public class SplashScreenActivity extends AppCompatActivity {
    String idProfile, idAkun, nama, idJabatan, nik, jk, tempatLahir, noHp, email, foto, alamat, username, pass, level, status, jabatan;
    Date tglLahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        if (android.os.Build.VERSION.SDK_INT > 14) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        String url = URL.getDataUser;
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        try {
            params.put("id_dinas", new UserSession(getApplicationContext()).getIdAkun());
            params.put("id_jabatan", new UserSession(getApplicationContext()).getIdJabatan());
        } catch (Exception e){
            e.printStackTrace();
            Log.e("error", String.valueOf(e instanceof Exception));
        }

        client.post(url, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                Log.e("status success", String.valueOf(statusCode));
                try {
                    JSONObject datas = new JSONObject(new String(responseBody));
                    Log.e("dataUSer", datas.toString());
                    if (datas.getInt("response") == 1 && statusCode == 200){
                        JSONArray profileUser = datas.getJSONArray("data");
                        for (int i = 0; i < profileUser.length(); i++) {
                            JSONObject x = profileUser.getJSONObject(i);
                            idProfile = x.getString("id_profile_dinas");
                            idAkun = x.getString("id_akun");
                            idJabatan = x.getString("id_jabatan");
                            nik = x.getString("nik");
                            nama = x.getString("nama");
                            jk = x.getString("jk");
                            tempatLahir = x.getString("tempat_lahir");
                            tglLahir = Date.valueOf(x.getString("tgl_lahir"));
                            noHp = x.getString("no_hp");
                            email = x.getString("email");
                            foto = x.getString("foto");
                            alamat = x.getString("alamat");
                            username = x.getString("username");
                            pass = x.getString("password");
                            level = x.getString("level");
                            status = x.getString("status");
                            jabatan = x.getString("jabatan");
                        }
                        String bulan = new SimpleDateFormat("MMMM").format(tglLahir);
                        String tgl = new SimpleDateFormat("dd").format(tglLahir);
                        String thn = new SimpleDateFormat("yyyy").format(tglLahir);
                        String ttl = tempatLahir + ", " + tgl + " " + bulan + " " + thn;

                        Log.e("ttl", ttl);

                        UserSession x = new UserSession(SplashScreenActivity.this);
                        x.setIsLogin(true);
                        x.setIdProfileDinas(idProfile);
                        x.setIdAkun(idAkun);
                        x.setIdJabatan(idJabatan);
                        x.setNik(nik);
                        x.setNama(nama);
                        x.setJk(jk);
                        x.setTtl(ttl);
                        x.setNoHp(noHp);
                        x.setEmail(email);
                        if (foto.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                            x.setFoto(null);
                        } else {
                            x.setFoto(foto);
                        }
                        Log.e("alamat", String.valueOf(alamat.length()));
                        if (alamat.equals("null") || foto.equals(null) || foto.equals("") || foto.isEmpty()){
                            x.setAlamat(null);
                        } else {
                            x.setAlamat(alamat);
                        }
                        x.setUsername(username);
                        x.setLevel(level);
                        x.setStatus(status);
                        x.setJabatan(jabatan);

                        splashScreenDelay();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Log.e("status error", String.valueOf(statusCode));
                Log.e("error", error.toString());
                if (statusCode == 400) {
                    try {
                        JSONObject datas = new JSONObject(new String(responseBody));
                        Log.e("loginUSer", datas.toString());
                        if (datas.getInt("response") == 0){
                            new UserSession(getApplicationContext()).clearSession();
                            splashScreenDelay();
                        } else {
                            splashScreenDelay();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    splashScreenDelay();
                }
            }
        });

    }

    private void splashScreenDelay() {
        final int welcomeScreenDisplay = 3000; // 3000 = 3 detik
        Thread welcomeThread = new Thread() {
            int wait = 0;
            @Override
            public void run() {
                try {
                    super.run();
                    while (wait < welcomeScreenDisplay) {
                        sleep(100);
                        wait += 100;
                    }
                } catch (Exception e) {
                    System.out.println("EXc=" + e);
                } finally {
//                    if (new UserSession(getApplicationContext()).getIsLogin()){
//                        OrderPreference o = new OrderPreference(getApplicationContext());
//                        String status = o.getStatus();
//                        if (status.equals("1")){
//                            Intent intent = new Intent(SplashScreenActivity.this, OrderSebelumRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else if (status.equals("2")) {
//                            Intent intent = new Intent(SplashScreenActivity.this, OrderSetelahRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else if (status.equals("3")) {
//                            Intent intent = new Intent(SplashScreenActivity.this, RangkumanRideActivity.class);
////                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        } else {
//                            Intent intent = new Intent(SplashScreenActivity.this, DashboardActivity.class);
//                            intent.putExtra(KEY_CONTENT, content);
//                            startActivity(intent);
//                        }
//                    } else {
//                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
//                        startActivity(intent);
//                    }
                    UserSession x = new UserSession(SplashScreenActivity.this);
                    if (x.getIsLogin()) {
                        Intent intent = new Intent(SplashScreenActivity.this, MainActivity.class);
                        startActivity(intent);
                    } else {
                        Intent intent = new Intent(SplashScreenActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                    finish();
                }
            }
        };
        welcomeThread.start();
    }
}
