package com.kilapdinas.kilapdinas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.ConnectionDetector;
import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.kilapdinas.kilapdinas.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class UbahPassActivity extends AppCompatActivity {
    @BindView(R.id.et_pass_lama)
    EditText etPassLama;
    @BindView(R.id.et_pass_baru)
    EditText etPassBaru;
    @BindView(R.id.et_konf_pass_baru)
    EditText etKonfPassBaru;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ubah_pass);
        ButterKnife.bind(this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("UBAH PASSWORD");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        //modal koneksi
        init_modal_bd_dialog();

        conn = new ConnectionDetector(getApplicationContext());
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    @OnClick(R.id.btn_pass)
    public void password(){
        if (etPassLama.getText().toString().isEmpty() || etPassBaru.getText().toString().isEmpty() || etKonfPassBaru.getText().toString().isEmpty()) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Silahkan lengkapi form profil untuk melanjutkan.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        } else if (!etPassBaru.getText().toString().equals(etKonfPassBaru.getText().toString()) || !etKonfPassBaru.getText().toString().equals(etPassBaru.getText().toString())) {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Silahkan periksa kembali password baru yang anda masukkan.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
            etPassBaru.setText(null);
            etKonfPassBaru.setText(null);
        } else {
            final ProgressDialog pDialog = new ProgressDialog(UbahPassActivity.this);
            pDialog.setCancelable(false);
            pDialog.setMessage("Tunggu Sebentar...");
            pDialog.show();
            if (conn.isConnected()) {
                String url = URL.ubahPasswordDinas;
                AsyncHttpClient client = new AsyncHttpClient();
                RequestParams params = new RequestParams();
                try {
                    params.put("id_akun", new UserSession(getApplicationContext()).getIdAkun());
                    params.put("pass_lama", etPassLama.getText().toString().trim());
                    params.put("pass_baru", etKonfPassBaru.getText().toString().trim());
                } catch (Exception e){
                    e.printStackTrace();
                    Log.e("error", String.valueOf(e instanceof Exception));
                }

                client.post(url, params, new AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        Log.e("status success", String.valueOf(statusCode));
                        try {
                            JSONObject datas = new JSONObject(new String(responseBody));
                            Log.e("loginUSer", datas.toString());
                            if (datas.getInt("response") == 1 && statusCode == 200){
//                                Log.e("nama", etNmLengkap.getText().toString().trim());
//                                UserSession x = new UserSession(getApplicationContext());
//                                x.setNama(etNmLengkap.getText().toString().trim());
//                                x.setJk(spJnsKelamin.getSelectedItem().toString().trim());
//                                x.setEmail(etEmail.getText().toString().trim());
//                                x.setNoHp(etNoHp.getText().toString().trim());
//                                x.setUsername(etUsername.getText().toString().trim());
//                                x.setAlamat(etALamat.getText().toString().trim());
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_check_green_24dp);
                                dialog.setTitle("Sukses");
                                dialog.setMessage(datas.getString("message"));
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        finish();
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.setNegativeButton("Tetap disini", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int which) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        etPassLama.setText(null);
                        etPassBaru.setText(null);
                        etKonfPassBaru.setText(null);
                        pDialog.dismiss();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        Log.e("status error", String.valueOf(statusCode));
                        Log.e("error", error.toString());
                        if (statusCode == 400) {
                            try {
                                JSONObject datas = new JSONObject(new String(responseBody));
                                Log.e("loginUSer", datas.toString());
                                if (datas.getInt("response") == 0){
                                    Log.e("status", String.valueOf(statusCode));
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage(datas.getString("message"));
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                            final AlertDialog.Builder dialog = new AlertDialog.Builder(UbahPassActivity.this);
                            dialog.setCancelable(false);
                            dialog.setIcon(R.drawable.ic_alert);
                            dialog.setTitle("Kesalahan");
                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            dialog.show();
                        } else {
                            bsdConn.show();
                        }
                        pDialog.dismiss();
                    }

                });
            } else {
                bsdConn.show();
                pDialog.dismiss();
            }
        }
    }
}
