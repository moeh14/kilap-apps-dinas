package com.kilapdinas.kilapdinas.ServerSide;

public class URL {
    public static final String root = "http://kilapapi.bdigy.com/";
    public static final String rootImage = "http://kilapsite.bdigy.com/assets/upload/image/";
    public static final String loginDinas = root+"loginDinas";
    public static final String getDataUser = root+"dataDinas";
    public static final String getLaporan = root+"dataLaporanDinas";
    public static final String getHistory = root+"/historyDinas/";
    public static final String ulasanTidakDiterima = root+"ulasanTidakDiterima";
    public static final String terimaLaporan = root+"terimaLaporan";
    public static final String tindaklanjuti = root+"tindaklanjuti";
    public static final String selesaiTindak = root+"selsesaiTindakLanjuti";
    public static final String laporanMasuk = root+"laporanMasuk";
    public static final String laporanProses = root+"laporanProses/";
    public static final String ubahPasswordDinas = root+"ubahPasswordDinas";
    public static final String ambilOrderan = root+"ambilOrderan/";
    public static final String mulaiPekerjaan = root+"mulaiPekerjaan/";
    public static final String orderanSelesai = root+"orderanSelesai/";
    public static final String dataPesanan = root+"dataPesanan/";
    //public static final String mejaPesan = root+"mejaPesan.php";
//    public static final String root_image_makan = rootImage+"dist/img/";
    public static final int TIMEOUT_ACCESS = 10000;
}