package com.kilapdinas.kilapdinas.ServerSide;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    // Logcat tag
    private static final String LOG = "DatabaseHelper";
    SQLiteDatabase db;
    ContentResolver contentResolver;

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "MILLI";

    // Table Names
    public static final String TABLE_MASTER = "TB_MASTER";
    public static final String TABLE_MESIN = "MESIN";
    public static final String TABLE_VERSION_UPDATE = "VERSION_UPDATE";
    public static final String TABLE_LOG_PENGIRIMAN = "LOG_PENGIRIMAN";

    // Common column names
    public static final String KEY_ID = "ID";
    public static final String KEY_COMPANY_ID = "COMPANY_ID";
    public static final String KEY_MILLCODE = "MILLCODE";
    public static final String KEY_CREATED_AT = "TDATE";
    public static final String KEY_STATION = "STATION";
    public static final String KEY_UNIT = "UNIT";
    public static final String KEY_UNITCODE = "UNITCODE";
    public static final String KEY_TYPECODE = "TYPECODE";

    // MASTER Table - column names
    public static final String KEY_UNITNAME = "UNITNAME";
    public static final String KEY_STD_PARAM = "STD_PARAM";
    public static final String KEY_UPPER_LIMIT = "UPPER_LIMIT";
    public static final String KEY_LOWER_LIMIT = "LOWER_LIMIT";
    public static final String KEY_UOM = "UOM";
    public static final String KEY_NOM = "NOM";
    public static final String KEY_ACTIVE = "ACTIVE";
    public static final String KEY_QR_CODE = "QR_CODE";

    // MESIN Table - column names
    public static final String KEY_SHIFT = "SHIFT";
    public static final String KEY_HOURS = "HOURS";
    public static final String KEY_SUBTYPECODE = "SUBTYPECODE";
    public static final String KEY_ACTUAL = "ACTUAL";
    public static final String KEY_IMAGE1 = "FILENAME_1";
    public static final String KEY_IMAGE2 = "FILNAME_2";
    public static final String KEY_CREATE_BY = "CREATE_BY";
    public static final String KEY_CREATE_DATE = "CREATE_DATE";
    public static final String KEY_UPDATE_BY = "UPDATE_BY";
    public static final String KEY_UPDATE_DATE = "UPDATE_DATE";
    public static final String KEY_SERIAL_NUMBER = "SERIAL_NUMBER";

    // VERSION_UPDATE Table = colmn names
    public static final String KEY_VERSION = "VERSION";

    // LOG_PENGIRIMAN - column
    public static final String KEY_LOG_STATUS = "LOG_STATUS";


    // NOTE_TAGS Table - column names
//    private static final String KEY_TODO_ID = "todo_id";
//    private static final String KEY_TAG_ID = "tag_id";

    // Table Create Statements
    // MASTER table create statement
    private static final String CREATE_TABLE_MASTER = "CREATE TABLE "
            + TABLE_MASTER + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_COMPANY_ID + " TEXT,"
            + KEY_UNITCODE + " TEXT,"
            + KEY_UNITNAME + " TEXT,"
            + KEY_STATION + " TEXT,"
            + KEY_UNIT + " TEXT,"
            + KEY_TYPECODE + " TEXT,"
            + KEY_STD_PARAM + " TEXT,"
            + KEY_UPPER_LIMIT + " TEXT,"
            + KEY_LOWER_LIMIT + " TEXT,"
            + KEY_UOM + " TEXT,"
            + KEY_NOM + " TEXT,"
            + KEY_ACTIVE + " TEXT,"
            + KEY_QR_CODE + " TEXT,"
            + KEY_MILLCODE + " TEXT,"
            + KEY_CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";

    // MESIN table create statement
    private static final String CREATE_TABLE_MESIN = "CREATE TABLE "
            + TABLE_MESIN + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_COMPANY_ID + " TEXT NOT NULL,"
            + KEY_MILLCODE + " TEXT NOT NULL,"
            + KEY_CREATED_AT + " TEXT NOT NULL,"
            + KEY_SHIFT + " TEXT NOT NULL,"
            + KEY_HOURS + " TEXT NOT NULL,"
            + KEY_STATION + " TEXT NOT NULL,"
            + KEY_UNIT + " TEXT NOT NULL,"
            + KEY_UNITCODE + " TEXT NOT NULL,"
            + KEY_TYPECODE + " TEXT NOT NULL,"
            + KEY_SUBTYPECODE + " TEXT NOT NULL,"
            + KEY_ACTUAL + " DOUBLE NOT NULL,"
            + KEY_IMAGE1 + " TEXT,"
            + KEY_IMAGE2+ " TEXT,"
            + KEY_CREATE_BY + " TEXT NOT NULL,"
            + KEY_CREATE_DATE + " TEXT NOT NULL,"
            + KEY_UPDATE_BY + " TEXT NOT NULL,"
            + KEY_UPDATE_DATE + " TEXT NOT NULL,"
            + KEY_SERIAL_NUMBER + " TEXT NOT NULL" + ")";

    // UPDATE_VERSION table create statement
    private static final String CREATE_TABLE_VERSION_UPDATE = "CREATE TABLE "
            + TABLE_VERSION_UPDATE + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_VERSION + " TEXT NOT NULL,"
            + KEY_CREATED_AT + " TIMESTAMP DEFAULT CURRENT_TIMESTAMP" + ")";

    // LOG_PENGIRIMAN table create statement
    private static final String CREATE_TABLE_LOG_PENGIRIMAN = "CREATE TABLE "
            + TABLE_LOG_PENGIRIMAN + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + KEY_LOG_STATUS + " TEXT NOT NULL,"
            + KEY_CREATED_AT + " TEXT NOT NULL" + ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        contentResolver = context.getContentResolver();

        db = this.getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_MASTER);
        db.execSQL(CREATE_TABLE_MESIN);
        db.execSQL(CREATE_TABLE_VERSION_UPDATE);
        db.execSQL(CREATE_TABLE_LOG_PENGIRIMAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MASTER);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MESIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VERSION_UPDATE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOG_PENGIRIMAN);
        onCreate(db);
    }

    //method insert
    public boolean insertVersion(String version){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_VERSION, version);

        db.insert(TABLE_VERSION_UPDATE, KEY_ID, c);
        return true;
    }

    //method insert
    public boolean insertLogPengiriman(String log, String tdate){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_LOG_STATUS, log);
        c.put(KEY_CREATED_AT, tdate);

        db.insert(TABLE_LOG_PENGIRIMAN, KEY_ID, c);
        return true;
    }

    //method insert master
    public boolean insertMaster(String companyId, String unitcode, String unitname, String station,
                                String unit, String typecode, String stdParam, String upperLimit,
                                String lowerLimit, String uom, String nom, String active, String qrCode,
                                String millcode) {
        Log.e("Millcode gw", station+" "+millcode);
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_COMPANY_ID, companyId);
        c.put(KEY_UNITCODE, unitcode);
        c.put(KEY_UNITNAME, unitname);
        c.put(KEY_STATION, station);
        c.put(KEY_UNIT, unit);
        c.put(KEY_TYPECODE, typecode);
        c.put(KEY_STD_PARAM, stdParam);
        c.put(KEY_UPPER_LIMIT, upperLimit);
        c.put(KEY_LOWER_LIMIT, lowerLimit);
        c.put(KEY_UOM, uom);
        c.put(KEY_NOM, nom);
        c.put(KEY_ACTIVE, active);
        c.put(KEY_QR_CODE, qrCode);
        c.put(KEY_MILLCODE, millcode);

        db.insert(TABLE_MASTER, null, c);
        return true;
    }

    //method insert mesin
    public boolean insertMesin(String companyId, String millcode, String tdate, String shift,
                               String hours, String station, String unit, String unitcode,
                               String typecode, String subtypecode, double actual, String image1,
                               String image2, String createBy, String createDate, String updateBy,
                               String updateDate, String serialNumber) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_COMPANY_ID, companyId);
        c.put(KEY_MILLCODE, millcode);
        c.put(KEY_CREATED_AT, tdate);
        c.put(KEY_SHIFT, shift);
        c.put(KEY_HOURS, hours);
        c.put(KEY_STATION, station);
        c.put(KEY_UNIT, unit);
        c.put(KEY_UNITCODE, unitcode);
        c.put(KEY_TYPECODE, typecode);
        c.put(KEY_SUBTYPECODE, subtypecode);
        c.put(KEY_ACTUAL, actual);
        c.put(KEY_IMAGE1, image1);
        c.put(KEY_IMAGE2, image2);
        c.put(KEY_CREATE_BY, createBy);
        c.put(KEY_CREATE_DATE, createDate);
        c.put(KEY_UPDATE_BY, updateBy);
        c.put(KEY_UPDATE_DATE, updateDate);
        c.put(KEY_SERIAL_NUMBER, serialNumber);

        db.insert(TABLE_MESIN, KEY_ID, c);
        return true;
    }

    //method update
    public boolean updatetMaster(String id, String companyId, String unitcode, String unitname, String station,
                                String unit, String typecode, String stdParam, String upperLimit,
                                String lowerLimit, String uom, String nom, String active, String qrCode,
                                String millcode) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID, id);
        c.put(KEY_COMPANY_ID, companyId);
        c.put(KEY_UNITCODE, unitcode);
        c.put(KEY_UNITNAME, unitname);
        c.put(KEY_STATION, station);
        c.put(KEY_UNIT, unit);
        c.put(KEY_TYPECODE, typecode);
        c.put(KEY_STD_PARAM, stdParam);
        c.put(KEY_UPPER_LIMIT, upperLimit);
        c.put(KEY_LOWER_LIMIT, lowerLimit);
        c.put(KEY_UOM, nom);
        c.put(KEY_NOM, nom);
        c.put(KEY_ACTIVE, active);
        c.put(KEY_QR_CODE, qrCode);
        c.put(KEY_MILLCODE, millcode);

        db.update(TABLE_MASTER, c, "ID = ?", new String[]{id});
        return true;
    }

    //method update mesin
    public boolean updateMesin(String id, String companyId, String millcode, String tdate, String shift,
                               String hours, String station, String unit, String unitcode,
                               String typecode, String subtypecode, String actual, byte[] image1,
                               byte[] image2) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues c = new ContentValues();
        c.put(KEY_ID, id);
        c.put(KEY_COMPANY_ID, companyId);
        c.put(KEY_MILLCODE, millcode);
        c.put(KEY_CREATED_AT, tdate);
        c.put(KEY_SHIFT, shift);
        c.put(KEY_HOURS, hours);
        c.put(KEY_STATION, station);
        c.put(KEY_UNIT, unit);
        c.put(KEY_UNITCODE, unitcode);
        c.put(KEY_TYPECODE, typecode);
        c.put(KEY_SUBTYPECODE, subtypecode);
        c.put(KEY_ACTUAL, actual);
        c.put(KEY_IMAGE1, image1);
        c.put(KEY_IMAGE2, image2);

        db.update(TABLE_MESIN, c, "ID = ?", new String[]{id});
        return true;
    }

    //method delete master
    public boolean deleteMaster(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MASTER, null, null);
        return true;
    }

    //method delete mesin
    public boolean deleteMesin(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_MESIN, null, null);
        return true;
    }

    public List<String> getAllStation(String millcode){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_STATION};
//        Cursor cursor = db.query(true, TABLE_MASTER, columns, "MILLCODE=?", new String[] { millcode }, null, null, KEY_STATION +" ASC", null);
        Cursor cursor = db.query(true, TABLE_MASTER, columns, null, null, null, null, KEY_STATION +" ASC", null);
        Log.e("data master", String.valueOf(cursor.getCount()));

        labels.add(0, "Pilih Station");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getAllUnitcode(String millcode, String station){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_UNITCODE};
        Cursor cursor = db.query(TABLE_MASTER, columns, "STATION=?", new String[] {station}, null, null, KEY_UNITCODE +" ASC");
        labels.add(0, "Pilih Unitcode");

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getVrsion(){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_VERSION};
        Cursor cursor = db.query(TABLE_VERSION_UPDATE, columns, null, null, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }

    public List<String> getFilterData(String station, String unitcode, String millcode){
        List<String> labels = new ArrayList<String>();

        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {KEY_UNITCODE};
        Cursor cursor = db.query(TABLE_MASTER, columns, "STATION=? AND UNITCODE=? AND MILLCODE=?", new String[] { station, unitcode, millcode }, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()){
            do {
                labels.add(cursor.getString(0));
            } while (cursor.moveToNext());
        }

        // closing connection
        cursor.close();
        db.close();

        //returning labels
        return labels;
    }
}
