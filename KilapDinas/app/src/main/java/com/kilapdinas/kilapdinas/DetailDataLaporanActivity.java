package com.kilapdinas.kilapdinas;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.provider.Settings;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.ConnectionDetector;
import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.kilapdinas.kilapdinas.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class DetailDataLaporanActivity extends AppCompatActivity {
    @BindView(R.id.tv_id)
    TextView tvId;
    @BindView(R.id.tv_kat)
    TextView tvKat;
    @BindView(R.id.tv_jns)
    TextView tvJns;
    @BindView(R.id.tv_pelapor)
    TextView tvPelapor;
    @BindView(R.id.tv_alamat)
    TextView tvAlamat;
    @BindView(R.id.tv_ket)
    TextView tvKet;
    @BindView(R.id.iv_bencana)
    ImageView ivBencana;
    @BindView(R.id.tv_tgl_bencana)
    TextView tvTglBencana;
    @BindView(R.id.tv_tgl_lapor)
    TextView tvTglLapor;
    @BindView(R.id.pb_load_image)
    ProgressBar pbLoad;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;
    private String id, kat, jns, pelapor, alamat, ket, image, tglBencana, tglLapor, idSurvey, nama, tglHasil, ketHasil, imageHasil;
    private int status;
    double lat, lng;
    private SimpleDateFormat dateFormat;
    Date date, date1, date2;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_data_laporan);
        ButterKnife.bind(this);

        //modal koneksi
        init_modal_bd_dialog();
        conn = new ConnectionDetector(DetailDataLaporanActivity.this);

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("DETAIL LAPORAN");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Locale locale = new Locale("in");
        dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", locale);

        Intent intent = getIntent();
        id = intent.getExtras().getString("id");
        kat = intent.getExtras().getString("kat");
        jns = intent.getExtras().getString("jns");
        pelapor = intent.getExtras().getString("nama_user");
        alamat = intent.getExtras().getString("alamat");
        lat = intent.getExtras().getDouble("lat");
        lng = intent.getExtras().getDouble("lng");
        ket = intent.getExtras().getString("ket");
        image = intent.getExtras().getString("image");
        tglBencana = intent.getExtras().getString("tgl_bencana");
        tglLapor = intent.getExtras().getString("tgl_lapor");
        try {
            Date date = dateFormat.parse(intent.getExtras().getString("tgl_bencana"));
            Date date1 = dateFormat.parse(intent.getExtras().getString("tgl_lapor"));
            dateFormat.applyPattern("dd MMMM yyyy HH:mm");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        idSurvey = intent.getExtras().getString("id_survey");

        tvId.setText(id);
        tvKat.setText(kat);
        tvJns.setText(jns);
        tvPelapor.setText(pelapor);
        tvAlamat.setText(alamat);
        tvKet.setText(ket);
        tvTglBencana.setText(dateFormat.format(date));
        tvTglLapor.setText(dateFormat.format(date1));
        Picasso.with(getApplicationContext()).load(URL.rootImage + image)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(ivBencana, new Callback() {
                    @Override
                    public void onSuccess() {
//                        holder.loadingImage.setVisibility(View.GONE);
                        pbLoad.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        holder.loadingImage.setVisibility(View.GONE);
                        pbLoad.setVisibility(View.GONE);
                        ivBencana.setImageResource(R.drawable.ic_no_image);
                    }
                });
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

    private void modalFormBatal() {
        AlertDialog.Builder builder = new AlertDialog.Builder(DetailDataLaporanActivity.this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.dialog_ulasan, null);
        builder.setView(v);
        final AlertDialog dialogBatal = builder.create();
        dialogBatal.setCancelable(false);
        dialogBatal.setCanceledOnTouchOutside(false);

        final EditText etUlasan = (EditText) v.findViewById(R.id.et_ulasan);
        Button btnKirim = (Button) v.findViewById(R.id.btn_kirim);
        btnKirim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUlasan.getText().toString().trim().isEmpty()){
                    etUlasan.setError("Harap diisi");
                } else {
                    final ProgressDialog pDialog = new ProgressDialog(DetailDataLaporanActivity.this);
                    pDialog.setCancelable(false);
                    pDialog.setMessage("Tunggu Sebentar...");
                    pDialog.show();
                    if (conn.isConnected()) {
                        String url = URL.ulasanTidakDiterima;
                        AsyncHttpClient client = new AsyncHttpClient();
                        RequestParams params = new RequestParams();
                        try {
                            params.put("id_dinas", new UserSession(getApplicationContext()).getIdAkun());
                            params.put("id_survey", idSurvey);
                            params.put("ulasan", etUlasan.getText().toString().trim());
                        } catch (Exception e){
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e instanceof Exception));
                        }

                        client.post(url, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                Log.e("status success", String.valueOf(statusCode));
                                try {
                                    JSONObject datas = new JSONObject(new String(responseBody));
                                    Log.e("loginUSer", datas.toString());
                                    if (datas.getInt("response") == 1 && statusCode == 200){
                                        final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setIcon(R.drawable.ic_check_green_24dp);
                                        dialog.setTitle("Sukses");
                                        dialog.setMessage(datas.getString("message"));
                                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                finish();
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        dialog.show();

                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                etUlasan.setText(null);
                                dialogBatal.dismiss();
                                pDialog.dismiss();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.e("status error", String.valueOf(statusCode));
                                Log.e("error", error.toString());
                                if (statusCode == 400) {
                                    try {
                                        JSONObject datas = new JSONObject(new String(responseBody));
                                        Log.e("loginUSer", datas.toString());
                                        if (datas.getInt("response") == 0){
                                            Log.e("status", String.valueOf(statusCode));
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                            dialog.setCancelable(false);
                                            dialog.setIcon(R.drawable.ic_alert);
                                            dialog.setTitle("Kesalahan");
                                            dialog.setMessage(datas.getString("message"));
                                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            dialog.show();
                                        } else {
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                            dialog.setCancelable(false);
                                            dialog.setIcon(R.drawable.ic_alert);
                                            dialog.setTitle("Kesalahan");
                                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            dialog.show();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    bsdConn.show();
                                }
                                pDialog.dismiss();
                            }

                        });
                    } else {
                        bsdConn.show();
                        dialogBatal.dismiss();
                        pDialog.dismiss();
                    }
                }
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBatal.setCancelable(true);
                dialogBatal.dismiss();
            }
        });


        dialogBatal.show();
    }

    @OnClick(R.id.rl_alamat)
    public void alamat(){
        Uri gmmIntentUri = Uri.parse("geo:"+lat+","+lng+"?q="+lat+","+lng+"(lokasi bencana)");
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    @OnClick(R.id.btn_tidak)
    public void btnBatal() {
        modalFormBatal();
    }

    @OnClick(R.id.btn_terima)
    public void btnTerima() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Pemberitahuan");
        dialog.setMessage("Apakah anda yakin ingin terima laporan bencana ini?.");
        dialog.setPositiveButton("Iya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                final ProgressDialog pDialog = new ProgressDialog(DetailDataLaporanActivity.this);
                pDialog.setCancelable(false);
                pDialog.setMessage("Tunggu Sebentar...");
                pDialog.show();
                if (conn.isConnected()) {
                    String url = URL.terimaLaporan;
                    AsyncHttpClient client = new AsyncHttpClient();
                    RequestParams params = new RequestParams();
                    try {
                        params.put("id_dinas", new UserSession(getApplicationContext()).getIdAkun());
                        params.put("id_survey", idSurvey);
                    } catch (Exception e){
                        e.printStackTrace();
                        Log.e("error", String.valueOf(e instanceof Exception));
                    }

                    client.post(url, params, new AsyncHttpResponseHandler() {
                        @Override
                        public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                            Log.e("status success", String.valueOf(statusCode));
                            try {
                                JSONObject datas = new JSONObject(new String(responseBody));
                                Log.e("loginUSer", datas.toString());
                                if (datas.getInt("response") == 1 && statusCode == 200){
                                    JSONArray data = datas.getJSONArray("data");
                                    for (int i = 0; i < data.length(); i++) {
                                        JSONObject x = data.getJSONObject(i);
                                        status = x.getInt("status_survey");
                                        tglHasil = x.getString("tgl_survey");

                                    }
                                    if (status == 1) {
                                        Intent intent = new Intent(DetailDataLaporanActivity.this, DetailHistoryTerimaActivity.class);
                                        intent.putExtra("id", id);
                                        intent.putExtra("kat", kat);
                                        intent.putExtra("jns", jns);
                                        intent.putExtra("alamat", alamat);
                                        intent.putExtra("lat", lat);
                                        intent.putExtra("lng", lng);
                                        intent.putExtra("ket", ket);
                                        intent.putExtra("image", image);
                                        intent.putExtra("tgl_bencana", tglBencana);
                                        intent.putExtra("tgl_lapor", tglLapor);
                                        intent.putExtra("id_survey", idSurvey);
                                        intent.putExtra("status", status);
                                        intent.putExtra("tgl_hasil", tglHasil);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            pDialog.dismiss();
                        }

                        @Override
                        public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                            Log.e("status error", String.valueOf(statusCode));
                            Log.e("error", error.toString());
                            if (statusCode == 400) {
                                try {
                                    JSONObject datas = new JSONObject(new String(responseBody));
                                    Log.e("loginUSer", datas.toString());
                                    if (datas.getInt("response") == 0){
                                        Log.e("status", String.valueOf(statusCode));
                                        final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setIcon(R.drawable.ic_alert);
                                        dialog.setTitle("Kesalahan");
                                        dialog.setMessage(datas.getString("message"));
                                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        dialog.show();
                                    } else {
                                        final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setIcon(R.drawable.ic_alert);
                                        dialog.setTitle("Kesalahan");
                                        dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        dialog.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else if (statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException){
                                final AlertDialog.Builder dialog = new AlertDialog.Builder(DetailDataLaporanActivity.this);
                                dialog.setCancelable(false);
                                dialog.setIcon(R.drawable.ic_alert);
                                dialog.setTitle("Kesalahan");
                                dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                    }
                                });
                                dialog.show();
                            } else {
                                bsdConn.show();
                            }
                            pDialog.dismiss();
                        }

                    });
                } else {
                    bsdConn.show();
                    pDialog.dismiss();
                }
                dialogInterface.dismiss();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}
