package com.kilapdinas.kilapdinas.Model;

import com.google.gson.annotations.SerializedName;

public class ModelDataLaporan {
    @SerializedName("id_bencana")
    private String id_bencana;
    @SerializedName("id_akun")
    private String id_akun;
    @SerializedName("id_kat_bencana")
    private String id_kat_bencana;
    @SerializedName("id_jns_bencana")
    private String id_jns_bencana;
    @SerializedName("ket")
    private String ket;
    @SerializedName("alamat")
    private String alamat;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;
    @SerializedName("image")
    private String image;
    @SerializedName("tgl_bencana")
    private String tgl_bencana;
    @SerializedName("tgl_pelaporan")
    private String tgl_pelaporan;
    @SerializedName("nama_user")
    private String nama_user;
    @SerializedName("kat_bencana")
    private String kat_bencana;
    @SerializedName("jns_bencana")
    private String jns_bencana;
    @SerializedName("id_survey")
    private String id_survey;
    @SerializedName("id_akun_survey")
    private String id_akun_survey;
    @SerializedName("status_survey")
    private int status_survey;
    @SerializedName("ket_survey")
    private String ket_survey;
    @SerializedName("image_survey")
    private String image_survey;
    @SerializedName("tgl_survey")
    private String tgl_survey;
    @SerializedName("nama_dinas")
    private String nama_dinas;

    public String getId_bencana() {
        return id_bencana;
    }

    public void setId_bencana(String id_bencana) {
        this.id_bencana = id_bencana;
    }

    public String getId_akun() {
        return id_akun;
    }

    public void setId_akun(String id_akun) {
        this.id_akun = id_akun;
    }

    public String getId_kat_bencana() {
        return id_kat_bencana;
    }

    public void setId_kat_bencana(String id_kat_bencana) {
        this.id_kat_bencana = id_kat_bencana;
    }

    public String getId_jns_bencana() {
        return id_jns_bencana;
    }

    public void setId_jns_bencana(String id_jns_bencana) {
        this.id_jns_bencana = id_jns_bencana;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTgl_bencana() {
        return tgl_bencana;
    }

    public void setTgl_bencana(String tgl_bencana) {
        this.tgl_bencana = tgl_bencana;
    }

    public String getTgl_pelaporan() {
        return tgl_pelaporan;
    }

    public void setTgl_pelaporan(String tgl_pelaporan) {
        this.tgl_pelaporan = tgl_pelaporan;
    }

    public String getNama_user() {
        return nama_user;
    }

    public void setNama_user(String nama_user) {
        this.nama_user = nama_user;
    }

    public String getKat_bencana() {
        return kat_bencana;
    }

    public void setKat_bencana(String kat_bencana) {
        this.kat_bencana = kat_bencana;
    }

    public String getJns_bencana() {
        return jns_bencana;
    }

    public void setJns_bencana(String jns_bencana) {
        this.jns_bencana = jns_bencana;
    }

    public String getId_survey() {
        return id_survey;
    }

    public void setId_survey(String id_survey) {
        this.id_survey = id_survey;
    }

    public String getId_akun_survey() {
        return id_akun_survey;
    }

    public void setId_akun_survey(String id_akun_survey) {
        this.id_akun_survey = id_akun_survey;
    }

    public Integer getStatus_survey() {
        return status_survey;
    }

    public void setStatus_survey(int status_survey) {
        this.status_survey = status_survey;
    }

    public String getKet_survey() {
        return ket_survey;
    }

    public void setKet_survey(String ket_survey) {
        this.ket_survey = ket_survey;
    }

    public String getImage_survey() {
        return image_survey;
    }

    public void setImage_survey(String image_survey) {
        this.image_survey = image_survey;
    }

    public String getTgl_survey() {
        return tgl_survey;
    }

    public void setTgl_survey(String tgl_survey) {
        this.tgl_survey = tgl_survey;
    }

    public String getNama_dinas() {
        return nama_dinas;
    }

    public void setNama_dinas(String nama_dinas) {
        this.nama_dinas = nama_dinas;
    }
}
