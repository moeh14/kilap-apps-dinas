package com.kilapdinas.kilapdinas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.kilapdinas.kilapdinas.ServerSide.UserSession;
import com.squareup.picasso.Callback;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AkunFragment extends Fragment {

    public AkunFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.tv_nm_lengkap)
    TextView tvNmLengkap;
    @BindView(R.id.tv_nik_akun)
    TextView tvNik;
    @BindView(R.id.tv_jk_akun)
    TextView tvJk;
    @BindView(R.id.tv_ttl_akun)
    TextView tvTtl;
    @BindView(R.id.tv_jabatan_akun)
    TextView tvJabatan;
    @BindView(R.id.tv_username_akun)
    TextView tvUsername;
    @BindView(R.id.tv_email_akun)
    TextView tvEmail;
    @BindView(R.id.tv_nohp_akun)
    TextView tvNohp;
    @BindView(R.id.tv_alamat_akun)
    TextView tvAlamat;
    @BindView(R.id.civ_image_akun)
    ImageView imgFotoAkun;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_akun, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)getActivity()).tvTitleToolbar.setText("AKUN SAYA");
        ((MainActivity)getActivity()).getLaporanMasuk();

        UserSession x = new UserSession(getActivity());
        tvNmLengkap.setText(x.getNama());
        tvNik.setText(x.getNik());
        tvJk.setText(x.getJk());
        tvTtl.setText(x.getTtl());
        tvJabatan.setText(x.getJabatan());
        tvUsername.setText(x.getUsername());
        tvEmail.setText(x.getEmail());
        tvNohp.setText(x.getNoHp());
        if (x.getAlamat().isEmpty() || x.getAlamat().equals(null)){
            tvAlamat.setText("-");
        } else {
            tvAlamat.setText(x.getAlamat());
        }
        Picasso.with(getActivity()).load(URL.rootImage + x.getFoto())
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .memoryPolicy(MemoryPolicy.NO_CACHE)
                .into(imgFotoAkun, new Callback() {
                    @Override
                    public void onSuccess() {
//                        pbImage.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {
//                        holder.gambar.setImageDrawable(context.getResources().getDrawable(R.drawable.noimage));
//                        pbImage.setVisibility(View.GONE);
                        imgFotoAkun.setImageResource(R.drawable.avatar);
                    }
                });

        return view;
    }

    @OnClick(R.id.btn_keluar)
    public void btnKeluar() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
        dialog.setTitle("Logout");
        dialog.setIcon(R.drawable.ic_power_red_24dp);
        dialog.setMessage("Ingin keluar?");
        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new UserSession(getActivity()).clearSession();
                startActivity(new Intent(getActivity(), LoginActivity.class));
                ((MainActivity)getActivity()).finish();
                dialogInterface.dismiss();
            }
        });
        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        dialog.show();
    }

    @OnClick(R.id.rl_pass)
    public void pass(){
        Intent intent = new Intent(getActivity(), UbahPassActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        Log.e("akun", "resume)");
        super.onResume();
        ((MainActivity)getActivity()).getLaporanMasuk();
    }

    @Override
    public void onPause() {
        Log.e("akun", "pause");
        super.onPause();
    }
}
