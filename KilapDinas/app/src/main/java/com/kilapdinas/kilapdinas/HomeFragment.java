package com.kilapdinas.kilapdinas;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.kilapdinas.kilapdinas.ServerSide.UserSession;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import butterknife.BindView;
import butterknife.ButterKnife;
import cz.msebera.android.httpclient.Header;

public class HomeFragment extends Fragment {
    public HomeFragment() {
        // Required empty public constructor
    }
    @BindView(R.id.tv_masuk)
    TextView tvMasuk;
    @BindView(R.id.tv_proses)
    TextView tvProses;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)getActivity()).tvTitleToolbar.setText("KILAP APPS");
        ((MainActivity)getActivity()).getLaporanMasuk();
        getLaporanMasuk();
        getLaporanProses();
        return view;
    }

    private void getLaporanProses() {
        String url = URL.laporanProses+new UserSession(getActivity()).getIdAkun();
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject datas = new JSONObject(new String(responseBody));
                    Log.e("laporanProses", datas.toString());
                    int laporanProses = datas.getInt("data");
                    tvProses.setText(String.valueOf(laporanProses));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                tvProses.setText("0");
            }
        });
    }

    public void getLaporanMasuk() {
        String url = URL.laporanMasuk;
        AsyncHttpClient client = new AsyncHttpClient();

        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    JSONObject datas = new JSONObject(new String(responseBody));
                    Log.e("laporanMasuk", datas.toString());
                    int laporanMasuk = datas.getInt("data");
                    tvMasuk.setText(String.valueOf(laporanMasuk));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                tvMasuk.setText("0");
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getLaporanMasuk();
    }
}
