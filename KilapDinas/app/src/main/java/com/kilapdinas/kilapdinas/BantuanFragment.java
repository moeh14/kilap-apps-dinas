package com.kilapdinas.kilapdinas;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BantuanFragment extends Fragment {

    public BantuanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_bantuan, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)getActivity()).tvTitleToolbar.setText("BANTUAN");
        ((MainActivity)getActivity()).getLaporanMasuk();
        return view;
    }

    @OnClick(R.id.rl_akun)
    public void akun(){
        Intent intent = new Intent(getActivity(), MenuAkunActivity.class);
        startActivity(intent);
    }

    @OnClick({R.id.rl_beranda})
    public void beranda() {
        Intent intent = new Intent(getActivity(), MenuBerandaActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_history)
    public void history(){
        Intent intent = new Intent(getActivity(), MenuHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.rl_laporan)
    public void laporan(){
        Intent intent = new Intent(getActivity(), MenuLaporanActivity.class);
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity)getActivity()).getLaporanMasuk();
    }
}
