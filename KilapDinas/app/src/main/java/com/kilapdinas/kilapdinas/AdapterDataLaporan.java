package com.kilapdinas.kilapdinas;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kilapdinas.kilapdinas.Model.ModelDataLaporan;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterDataLaporan extends RecyclerView.Adapter<AdapterDataLaporan.HolderItem> {
    private Context context;
    private List<ModelDataLaporan> datas;
    public AdapterDataLaporan(Context context) {
        this.context = context;
    }

    @NonNull
    @Override
    public AdapterDataLaporan.HolderItem onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_data_laporan, null);
        HolderItem holderItem = new HolderItem(view);
        return holderItem;
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterDataLaporan.HolderItem holder, int i) {
        final ModelDataLaporan x = datas.get(i);

        holder.tvJns.setText(x.getJns_bencana());
        holder.tvAlamat.setText("Alamat bencana : "+x.getAlamat());
        holder.tvKat.setText("Kategori bencana : "+x.getKat_bencana());
        holder.llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailDataLaporanActivity.class);
                intent.putExtra("id", x.getId_bencana());
                intent.putExtra("kat", x.getKat_bencana());
                intent.putExtra("jns", x.getJns_bencana());
                intent.putExtra("nama_user", x.getNama_user());
                intent.putExtra("alamat", x.getAlamat());
                intent.putExtra("lat", x.getLat());
                intent.putExtra("lng", x.getLng());
                intent.putExtra("ket", x.getKet());
                intent.putExtra("image", x.getImage());
                intent.putExtra("tgl_bencana", x.getTgl_bencana());
                intent.putExtra("tgl_lapor", x.getTgl_pelaporan());
                intent.putExtra("id_survey", x.getId_survey());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datas.size();
    }

    public void setDatas(List<ModelDataLaporan> datas) {
        this.datas = datas;
    }

    public List<ModelDataLaporan> getDatas() {
        return datas;
    }

    public class HolderItem extends RecyclerView.ViewHolder {
        @BindView(R.id.ll_item)
        LinearLayout llItem;
        @BindView(R.id.tv_jns)
        TextView tvJns;
        @BindView(R.id.tv_alamat)
        TextView tvAlamat;
        @BindView(R.id.tv_kat)
        TextView tvKat;

        public HolderItem(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
