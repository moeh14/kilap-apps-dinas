package com.kilapdinas.kilapdinas;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.kilapdinas.kilapdinas.ServerSide.ConnectionDetector;
import com.kilapdinas.kilapdinas.ServerSide.URL;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.net.ssl.SSLException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpResponseException;

public class FormHasilTindakActivity extends AppCompatActivity {
    @BindView(R.id.et_ket_hasil)
    EditText etKetHasil;
    @BindView(R.id.iv_hasil)
    ImageView ivHasil;

    private Toolbar toolbarMain;
    private TextView tvTitleToolbar;

    ConnectionDetector conn;
    BottomSheetDialog bsdConn;

    private static final int SELECT_PHOTO = 1;
    private static final int CAPTURE_PHOTO = 2;
    private Bitmap thumbnail;
    private String id, kat, jns, alamat, ket, image, tglBencana, tglLapor, idSurvey, nama, tglHasil, ketHasil, imageHasil;
    private int status;
    private double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_hasil_tindak);
        ButterKnife.bind(this);

        conn = new ConnectionDetector(FormHasilTindakActivity.this);

        //modal koneksi
        init_modal_bd_dialog();

        Intent intent = getIntent();
        id = intent.getExtras().getString("id");
        kat = intent.getExtras().getString("kat");
        jns = intent.getExtras().getString("jns");
        alamat = intent.getExtras().getString("alamat");
        lat = intent.getExtras().getDouble("lat");
        lng = intent.getExtras().getDouble("lng");
        ket = intent.getExtras().getString("ket");
        image = intent.getExtras().getString("image");
        tglBencana = intent.getExtras().getString("tgl_bencana");
        tglLapor = intent.getExtras().getString("tgl_lapor");
        idSurvey = intent.getExtras().getString("id_survey");
        status =  intent.getExtras().getInt("status");
        tglHasil = intent.getExtras().getString("tgl_hasil");

        toolbarMain = (Toolbar) findViewById(R.id.toolbar_main);
        tvTitleToolbar = (TextView) toolbarMain.findViewById(R.id.tv_titile_toolbar);
        tvTitleToolbar.setText("DETAIL LAPORAN");
        toolbarMain.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp));
        toolbarMain.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FormHasilTindakActivity.this, DetailHistoryProsesActivity.class);
                intent.putExtra("id", id);
                intent.putExtra("kat", kat);
                intent.putExtra("jns", jns);
                intent.putExtra("alamat", alamat);
                intent.putExtra("lat", lat);
                intent.putExtra("lng", lng);
                intent.putExtra("ket", ket);
                intent.putExtra("image", image);
                intent.putExtra("tgl_bencana", tglBencana);
                intent.putExtra("tgl_lapor", tglLapor);
                intent.putExtra("id_survey", idSurvey);
                intent.putExtra("status", status);
                intent.putExtra("tgl_hasil", tglHasil);
                startActivity(intent);
                finish();
            }
        });
    }

    private void init_modal_bd_dialog() {
        View v = getLayoutInflater().inflate(R.layout.bd_koneksi, null);

        bsdConn = new BottomSheetDialog(this);
        bsdConn.setContentView(v);
        bsdConn.setCanceledOnTouchOutside(false);
        bsdConn.setCancelable(false);

        Button btnPengaturan = (Button) v.findViewById(R.id.btn_pengaturan);
        btnPengaturan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                startActivity(new Intent(Settings.ACTION_SETTINGS));
                bsdConn.dismiss();
            }
        });

        ImageButton ibClose = (ImageButton) v.findViewById(R.id.ib_close_conn);
        ibClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bsdConn.setCancelable(true);
                bsdConn.dismiss();
            }
        });
    }

//    @OnClick(R.id.btn_lapor_hasil)
//    public void btnHasil() {
//        final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
//        dialog.setCancelable(false);
//        dialog.setIcon(R.drawable.ic_alert);
//        dialog.setTitle("Pemberitahuan");
//        dialog.setMessage("Apakah anda yakin ingin mengirim hasil laporan ?.");
//        dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//            }
//        });
//        dialog.show();
//    }

    @OnClick(R.id.rl_upload_gambar)
    public void uploadGambar(){
        new MaterialDialog.Builder(this)
                .title(R.string.uploadImages)
                .items(R.array.uploadImages)
                .itemsIds(R.array.itemIds)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which){
                            case 0:
                                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(intent, CAPTURE_PHOTO);
                                break;
                            case 1:
                                ivHasil.setImageDrawable(null);
                                break;
                        }
                    }
                })
                .show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CAPTURE_PHOTO){
            if(resultCode == RESULT_OK) {
                onCaptureImageResult1(data);
                Log.e("data", String.valueOf(data));
            }
        }
    }

    private void onCaptureImageResult1(Intent data) {
        thumbnail = (Bitmap) data.getExtras().get("data");

        ivHasil.setImageBitmap(thumbnail);
    }

    @OnClick(R.id.btn_lapor_hasil)
    public void laporHasil(){
        if (etKetHasil.getText().toString().isEmpty() || thumbnail == null){
            final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
            dialog.setCancelable(false);
            dialog.setIcon(R.drawable.ic_alert);
            dialog.setTitle("Kesalahan");
            dialog.setMessage("Silahkan lengkapi form inputan hasil tindakan anda.");
            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            dialog.show();
        } else {
            final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
            dialog.setCancelable(false);
            dialog.setTitle("Lapor Bencana");
            dialog.setMessage("Apakah anda yakin ingin mengirim hasil laporan ?.");
            dialog.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    final ProgressDialog pDialog = new ProgressDialog(FormHasilTindakActivity.this);
                    pDialog.setCancelable(false);
                    pDialog.setMessage("Tunggu Sebentar...");
                    pDialog.show();
                    if (conn.isConnected()){
                        String url = URL.selesaiTindak;
                        AsyncHttpClient client= new AsyncHttpClient();
                        RequestParams params = new RequestParams();
                        try {
                            params.put("id_survey", idSurvey);
                            params.put("ket_survey", etKetHasil.getText().toString());
                            params.put("attachment", storeImage(thumbnail));
                        } catch (Exception e){
                            e.printStackTrace();
                            Log.e("error", String.valueOf(e instanceof FileNotFoundException));
                        }

                        client.post(url, params, new AsyncHttpResponseHandler() {
                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                try {
                                    JSONObject datas = new JSONObject(new String(responseBody));
                                    Log.e("lapor", datas.toString());
                                    JSONArray data = datas.getJSONArray("data");
                                    if (datas.getInt("response") == 1 && statusCode == 200) {
                                        for (int i = 0; i < data.length(); i++) {
                                            JSONObject x = data.getJSONObject(i);
                                            status = x.getInt("status_survey");
                                            ketHasil = x.getString("ket_survey");
                                            imageHasil = x.getString("image_survey");
                                            tglHasil = x.getString("tgl_survey");

                                        }

                                        final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
                                        dialog.setCancelable(false);
                                        dialog.setIcon(R.drawable.ic_check_green_24dp);
                                        dialog.setTitle("Sukses");
                                        dialog.setMessage(datas.getString("message"));
                                        dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intent = new Intent(FormHasilTindakActivity.this, DetailHistorySBActivity.class);
                                                intent.putExtra("id", id);
                                                intent.putExtra("kat", kat);
                                                intent.putExtra("jns", jns);
                                                intent.putExtra("alamat", alamat);
                                                intent.putExtra("lat", lat);
                                                intent.putExtra("lng", lng);
                                                intent.putExtra("ket", ket);
                                                intent.putExtra("image", image);
                                                intent.putExtra("tgl_bencana", tglBencana);
                                                intent.putExtra("tgl_lapor", tglLapor);
                                                intent.putExtra("id_survey", idSurvey);
                                                intent.putExtra("status", status);
                                                intent.putExtra("tgl_hasil", tglHasil);
                                                intent.putExtra("ket_hasil", ketHasil);
                                                intent.putExtra("image_hasil", imageHasil);
                                                startActivity(intent);
                                                finish();
                                                dialogInterface.dismiss();
                                            }
                                        });
                                        dialog.show();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                etKetHasil.setText(null);
                                ivHasil.setImageDrawable(null);
                                pDialog.dismiss();
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                                Log.e("status", String.valueOf(statusCode));
                                if (statusCode == 400) {
                                    try {
                                        JSONObject datas = new JSONObject(new String(responseBody));
                                        Log.e("lapor bencana error", datas.toString());
                                        if (datas.getInt("response") == 0) {
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
                                            dialog.setCancelable(false);
                                            dialog.setIcon(R.drawable.ic_alert);
                                            dialog.setTitle("Kesalahan");
                                            dialog.setMessage(datas.getString("message"));
                                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            dialog.show();
                                        } else {
                                            final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
                                            dialog.setCancelable(false);
                                            dialog.setIcon(R.drawable.ic_alert);
                                            dialog.setTitle("Kesalahan");
                                            dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                            dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            });
                                            dialog.show();
                                        }
                                        pDialog.dismiss();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                } else if(statusCode != 0 || error instanceof SSLException || error instanceof HttpResponseException) {
                                    final AlertDialog.Builder dialog = new AlertDialog.Builder(FormHasilTindakActivity.this);
                                    dialog.setCancelable(false);
                                    dialog.setIcon(R.drawable.ic_alert);
                                    dialog.setTitle("Kesalahan");
                                    dialog.setMessage("Terjadi kesalahan pada server, cobalah beberapa saat lagi.");
                                    dialog.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                                    dialog.show();
                                } else {
                                    bsdConn.show();
                                }
                                pDialog.dismiss();
                            }
                        });
                    } else {
                        bsdConn.show();
                        pDialog.dismiss();
                    }
                    dialogInterface.dismiss();
                }
            });
            dialog.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    }

    private File storeImage(Bitmap bitmap) {
        File pictureFile = getOutputMediaFile();
        if (pictureFile == null) {
            Log.d("file",
                    "Error creating media file, check storage permissions: ");// e.getMessage());
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
            fos.close();
        } catch (FileNotFoundException e) {
            Log.d("error file", "File not found: " + e.getMessage());
        } catch (IOException e) {
            Log.d("error file1", "Error accessing file: " + e.getMessage());
        }
        return pictureFile;
    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory()
                + "/Android/data/"
                + getApplicationContext().getPackageName()
                + "/Files");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName="MI_"+ timeStamp +".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }
}
